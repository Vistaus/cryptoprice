# Spanish translation for cryptoprice.maltekiefer.
# Copyright (C) 2019
# This file is distributed under the same license as the cryptoprice.maltekiefer package.
# advocatux <advocatux@airpost.net>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: cryptoprice.maltekiefer\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-30 09:37+0000\n"
"PO-Revision-Date: 2019-08-30 20:43+0200\n"
"Last-Translator: advocatux <advocatux@airpost.net>\n"
"Language-Team: UBports Spanish Team\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../qml/Settings.qml:13
msgid "Settings"
msgstr "Configuración"

#: ../qml/Settings.qml:31
msgid "Automatic Updates every 5 seconds?"
msgstr "¿Actualizar automáticamente cada 5 segundos?"

#: ../qml/Settings.qml:45
msgid "Your currency?"
msgstr "¿Su moneda?"

#: ../qml/Settings.qml:71
msgid "Source of prices?"
msgstr "¿Fuente de los precios?"

#: CryptoPrice.desktop.in.h:1
msgid "CryptoPrice"
msgstr "CryptoPrice"
