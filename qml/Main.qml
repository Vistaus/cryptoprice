/*
    ICONS: https://github.com/atomiclabs/cryptocurrency-icons/
*/

import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.3
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'cryptoprice.maltekiefer'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Settings {
        id: settings
        property bool automaticRefresh: false
        property bool showArrows: true
        property bool showLabels: false
        property int currentIndex: 4
        property int apiIndex: 0
        property string api: "COINBASE"
        property string userCurrency: "USD"
        property string userCurrencySymbol: "$"
        property string userCurrencySymbolPosition: "left"
    }

    Settings {
        id: curSettings
        property string btc: "0"
        property string eth: "0"
        property string xrp: "0"
        property string bch: "0"
        property string ltc: "0"
        property string eos: "0"
        property string xlm: "0"
        property string etc: "0"
    }

    Timer {
        id: timer
        interval: 5000
        repeat: settings.automaticRefresh
        running: settings.automaticRefresh

        onTriggered:
        {
            getPrice()
        }
    }

    PageStack {
        id: pageStack
        Component.onCompleted: pageStack.push(mainPage)

        Page {
            id: mainPage
            anchors.fill: parent

            header: PageHeader {
                id: header
                title: 'CryptoPrice'
                trailingActionBar {
                    actions: [
                        Action {
                            iconName: "settings"
                            text: "settings"

                            onTriggered: pageStack.push(Qt.resolvedUrl("Settings.qml"))
                        },
                        Action {
                            iconName: "reload"
                            text: "refresh"

                            onTriggered: getPrice()
                        }
                    ]
                }
            }
            Flickable {
                anchors.fill: parent
                anchors.top: parent.bottom
                anchors.topMargin: units.gu(6.3)
                contentHeight: main.height + units.gu(10) + main.anchors.topMargin
                id: flick
                GridLayout {
                    id: main
                    rowSpacing: units.gu(1.5)
                    columnSpacing: units.gu(1.5)
                    columns: 4
                    anchors.horizontalCenter: parent.horizontalCenter
                    Image {
                        source: "assets/btc.svg"
                        sourceSize: Qt.size( imgbtc.sourceSize.width*4, imgbtc.sourceSize.height*4 )
                        Image {
                            id: imgbtc
                            source: parent.source
                            width: 0
                            height: 0
                        }
                    }
                    Label {
                      font.weight: Font.Light
                      font.pixelSize: units.gu(2.5)
                      text: settings.showLabels ? "BTC" : ""
                      color: "#000000"
                    }
                    Label {
                        id: lb_bitcoin
                        Layout.preferredWidth: units.gu(27)
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: curSettings.btc
                        color: "#000000"
                    }
                    Label {
                        id: lb_bitcoinarrow
                        text: ""
                        horizontalAlignment: Text.AlignRight
                    }

                    Image {
                        source: "assets/eth.svg"
                        sourceSize: Qt.size( imgeth.sourceSize.width*4, imgeth.sourceSize.height*4 )
                        Image {
                            id: imgeth
                            source: parent.source
                            width: 0
                            height: 0
                        }
                    }
                    Label {
                      font.weight: Font.Light
                      font.pixelSize: units.gu(2.5)
                      text: settings.showLabels ? "ETH" : ""
                      color: "#000000"
                    }
                    Label {
                        id: lb_eth
                        Layout.preferredWidth: units.gu(27)
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: curSettings.eth
                        color: "#000000"
                    }
                    Label {
                        id: lb_etharrow
                        text: ""
                    }

                    Image {
                        source: "assets/xrp.svg"
                        sourceSize: Qt.size( imgxrp.sourceSize.width*4, imgxrp.sourceSize.height*4 )
                        Image {
                            id: imgxrp
                            source: parent.source
                            width: 0
                            height: 0
                        }
                    }
                    Label {
                      font.weight: Font.Light
                      font.pixelSize: units.gu(2.5)
                      text: settings.showLabels ? "XRP" : ""
                      color: "#000000"
                    }
                    Label {
                        id: lb_xrp
                        Layout.preferredWidth: units.gu(27)
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: curSettings.xrp
                        color: "#000000"
                    }
                    Label {
                        id: lb_xrparrow
                        text: ""
                    }

                    Image {
                        source: "assets/bch.svg"
                        sourceSize: Qt.size( imgbch.sourceSize.width*4, imgbch.sourceSize.height*4 )
                        Image {
                            id: imgbch
                            source: parent.source
                            width: 0
                            height: 0
                        }
                    }
                    Label {
                      font.weight: Font.Light
                      font.pixelSize: units.gu(2.5)
                      text: settings.showLabels ? "BCH" : ""
                      color: "#000000"
                    }
                    Label {
                        id: lb_bch
                         Layout.preferredWidth: units.gu(27)
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: curSettings.bch
                        color: "#000000"
                    }
                    Label {
                        id: lb_bcharrow
                        text: ""
                    }

                    Image {
                        source: "assets/ltc.svg"
                        sourceSize: Qt.size( imgltc.sourceSize.width*4, imgltc.sourceSize.height*4 )
                        Image {
                            id: imgltc
                            source: parent.source
                            width: 0
                            height: 0
                        }
                    }
                    Label {
                      font.weight: Font.Light
                      font.pixelSize: units.gu(2.5)
                      text: settings.showLabels ? "LTC" : ""
                      color: "#000000"
                    }
                    Label {
                        id: lb_ltc
                        Layout.preferredWidth: units.gu(27)
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: curSettings.ltc
                        color: "#000000"
                    }
                    Label {
                        id: lb_ltcarrow
                        text: ""
                    }

                    Image {
                        source: "assets/eos.svg"
                        sourceSize: Qt.size( imgeos.sourceSize.width*4, imgeos.sourceSize.height*4 )
                        Image {
                            id: imgeos
                            source: parent.source
                            width: 0
                            height: 0
                        }
                    }
                    Label {
                      font.weight: Font.Light
                      font.pixelSize: units.gu(2.5)
                      text: settings.showLabels ? "EOS" : ""
                      color: "#000000"
                    }
                    Label {
                        id: lb_eos
                        Layout.preferredWidth: units.gu(27)
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: curSettings.eos
                        color: "#000000"
                    }
                    Label {
                        id: lb_eosarrow
                        text: ""
                    }

                    Image {
                        source: "assets/xlm.svg"
                        sourceSize: Qt.size( imgxlm.sourceSize.width*4, imgxlm.sourceSize.height*4 )
                        Image {
                            id: imgxlm
                            source: parent.source
                            width: 0
                            height: 0
                        }
                    }
                    Label {
                      font.weight: Font.Light
                      font.pixelSize: units.gu(2.5)
                      text: settings.showLabels ? "XLM" : ""
                      color: "#000000"
                    }
                    Label {
                        id: lb_xlm
                        Layout.preferredWidth: units.gu(27)
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: curSettings.xlm
                        color: "#000000"
                    }
                    Label {
                        id: lb_xlmarrow
                        text: ""
                    }

                    Image {
                        source: "assets/etc.svg"
                        sourceSize: Qt.size( imgetc.sourceSize.width*4, imgetc.sourceSize.height*4 )
                        Image {
                            id: imgetc
                            source: parent.source
                            width: 0
                            height: 0
                        }
                    }
                    Label {
                      font.weight: Font.Light
                      font.pixelSize: units.gu(2.5)
                      text: settings.showLabels ? "ETC" : ""
                      color: "#000000"
                    }
                    Label {
                        id: lb_etc
                        Layout.preferredWidth: units.gu(27)
                        horizontalAlignment: Text.AlignRight
                        elide: Text.ElideRight
                        font.weight: Font.Light
                        font.pixelSize: units.gu(2.5)
                        text: curSettings.etc
                        color: "#000000"
                    }
                    Label {
                        id: lb_etcarrow
                        text: ""
                    }
                }
            }

            Component.onCompleted: {
                getPrice()
            }
        }
    }

    Python {
        id: python

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('./'));

            importModule('cryptoprice', function() {
                console.log("-------------- python loaded");
            });
        }

        onError: {
            console.log('python error: ' + traceback);
        }
    }

    function getPrice() {
        python.call('cryptoprice.get_latest_price', [settings.api, 'BTC', settings.userCurrency], function(returnValue) {
            if (settings.userCurrencySymbolPosition == "left")
                lb_bitcoin.text = "<b>" + settings.userCurrencySymbol + " " + (returnValue).toFixed(2) + "</b>"
            else
                lb_bitcoin.text = "<b>" + (returnValue).toFixed(2) + " " + settings.userCurrencySymbol + "</b>"
            if(settings.showArrows)
            {
                if(curSettings.btc == returnValue)
                    lb_bitcoinarrow.text = "\u2B0C"

                if(returnValue < curSettings.btc)
                    lb_bitcoinarrow.text = "\u2B07"

                if(returnValue > curSettings.btc)
                    lb_bitcoinarrow.text = "\u2B06"
            }
            else
                lb_bitcoinarrow.text = ""

            if(curSettings.btc == returnValue)
                lb_bitcoin.color = "#000000"

            if(returnValue < curSettings.btc)
                lb_bitcoin.color = "#e60000"

            if(returnValue > curSettings.btc)
                lb_bitcoin.color = "#009933"

            curSettings.btc = (returnValue).toFixed(2)

        })
        python.call('cryptoprice.get_latest_price', [settings.api, 'ETH', settings.userCurrency], function(returnValue) {
            if (settings.userCurrencySymbolPosition == "left")
                lb_eth.text = "<b>" + settings.userCurrencySymbol + " " + (returnValue).toFixed(2) + "</b>"
            else
                lb_eth.text = "<b>" + (returnValue).toFixed(2) + " " + settings.userCurrencySymbol + "</b>"

            if(settings.showArrows)
            {
                if(curSettings.eth == returnValue)
                    lb_etharrow.text = "\u2B0C"

                if(returnValue < curSettings.eth)
                    lb_etharrow.text = "\u2B07"

                if(returnValue > curSettings.eth)
                    lb_etharrow.text = "\u2B06"

            }
            else
                lb_etharrow.text = ""

            if(curSettings.eth == returnValue)
                lb_eth.color = "#000000"

            if(returnValue < curSettings.eth)
                lb_eth.color = "#e60000"

            if(returnValue > curSettings.eth)
                lb_eth.color = "#009933"

            curSettings.eth = (returnValue).toFixed(2)
        })
        python.call('cryptoprice.get_latest_price', [settings.api, 'XRP', settings.userCurrency], function(returnValue) {
            if (settings.userCurrencySymbolPosition == "left")
                lb_xrp.text = "<b>" + settings.userCurrencySymbol + " " + (returnValue).toFixed(2) + "</b>"
            else
                lb_xrp.text = "<b>" + (returnValue).toFixed(2) + " " + settings.userCurrencySymbol + "</b>"

            if(settings.showArrows)
            {
                if(curSettings.xrp == returnValue)
                    lb_xrparrow.text = "\u2B0C"

                if(returnValue < curSettings.xrp)
                    lb_xrparrow.text = "\u2B07"

                if(returnValue > curSettings.xrp)
                    lb_xrparrow.text = "\u2B06"
            }
            else
                lb_xrparrow.text = ""

            if(curSettings.eth == returnValue)
                lb_xrp.color = "#000000"

            if(returnValue < curSettings.xrp)
                lb_xrp.color = "#e60000"

            if(returnValue > curSettings.xrp)
                lb_xrp.color = "#009933"

            curSettings.xrp = (returnValue).toFixed(2)
        })
        python.call('cryptoprice.get_latest_price', [settings.api, 'BCH', settings.userCurrency], function(returnValue) {
            if (settings.userCurrencySymbolPosition == "left")
                lb_bch.text = "<b>" + settings.userCurrencySymbol + " " + (returnValue).toFixed(2) + "</b>"
            else
                lb_bch.text = "<b>" + (returnValue).toFixed(2) + " " + settings.userCurrencySymbol + "</b>"

            if(settings.showArrows)
            {
                if(curSettings.bch == returnValue)
                    lb_bcharrow.text = "\u2B0C"

                if(returnValue < curSettings.bch)
                   lb_bcharrow.text = "\u2B07"

                if(returnValue > curSettings.bch)
                    lb_bcharrow.text = "\u2B06"
            }
            else
                lb_bcharrow.text = ""

            if(curSettings.bch == returnValue)
                lb_bch.color = "#000000"

            if(returnValue < curSettings.bch)
                lb_bch.color = "#e60000"

            if(returnValue > curSettings.bch)
                lb_bch.color = "#009933"

            curSettings.bch = (returnValue).toFixed(2)
        })
        python.call('cryptoprice.get_latest_price', [settings.api, 'LTC', settings.userCurrency], function(returnValue) {
            if (settings.userCurrencySymbolPosition == "left")
                lb_ltc.text = "<b>" + settings.userCurrencySymbol + " " + (returnValue).toFixed(2) + "</b>"
            else
                lb_ltc.text = "<b>" + (returnValue).toFixed(2) + " " + settings.userCurrencySymbol + "</b>"

            if(settings.showArrows)
            {
                if(curSettings.ltc == returnValue)
                    lb_ltcarrow.text = "\u2B0C"

                if(returnValue < curSettings.ltc)
                    lb_ltcarrow.text = "\u2B07"

                if(returnValue > curSettings.ltc)
                    lb_ltcarrow.text = "\u2B06"
            }
            else
                lb_ltcarrow.text = ""

            if(curSettings.ltc == returnValue)
                lb_ltc.color = "#000000"

            if(returnValue < curSettings.ltc)
                lb_ltc.color = "#e60000"

            if(returnValue > curSettings.ltc)
                lb_ltc.color = "#009933"

            curSettings.ltc = (returnValue).toFixed(2)
        })

        python.call('cryptoprice.get_latest_price', [settings.api, 'EOS', settings.userCurrency], function(returnValue) {
            if (settings.userCurrencySymbolPosition == "left")
                lb_eos.text = "<b>" + settings.userCurrencySymbol + " " + (returnValue).toFixed(2) + "</b>"
            else
                lb_eos.text = "<b>" + (returnValue).toFixed(2) + " " + settings.userCurrencySymbol + "</b>"

            if(settings.showArrows)
            {
                if(curSettings.eos == returnValue)
                    lb_eosarrow.text = "\u2B0C"

                if(returnValue < curSettings.eos)
                    lb_eosarrow.text = "\u2B07"

                if(returnValue > curSettings.eos)
                    lb_eosarrow.text = "\u2B06"
            }
            else
                lb_eosarrow.text = ""

            if(curSettings.eos == returnValue)
                lb_eos.color = "#000000"

            if(returnValue < curSettings.eos)
                lb_eos.color = "#e60000"

            if(returnValue > curSettings.eos)
                lb_eos.color = "#009933"

            curSettings.eos = (returnValue).toFixed(2)
        })

        python.call('cryptoprice.get_latest_price', [settings.api, 'XLM', settings.userCurrency], function(returnValue) {
            if (settings.userCurrencySymbolPosition == "left")
                lb_xlm.text = "<b>" + settings.userCurrencySymbol + " " + (returnValue).toFixed(2) + "</b>"
            else
                lb_xlm.text = "<b>" + (returnValue).toFixed(2) + " " + settings.userCurrencySymbol + "</b>"

            if(settings.showArrows)
            {
                if(curSettings.xlm == returnValue)
                    lb_xlmarrow.text = "\u2B0C"

                if(returnValue < curSettings.xlm)
                    lb_xlmarrow.text = "\u2B07"

                if(returnValue > curSettings.xlm)
                    lb_xlmarrow.text = "\u2B06"
            }
            else
                lb_xlmarrow.text = ""

            if(curSettings.xlm == returnValue)
                lb_xlm.color = "#000000"

            if(returnValue < curSettings.xlm)
                lb_xlm.color = "#e60000"

            if(returnValue > curSettings.xlm)
                lb_xlm.color = "#009933"

            curSettings.xlm = (returnValue).toFixed(2)
        })

        python.call('cryptoprice.get_latest_price', [settings.api, 'ETC', settings.userCurrency], function(returnValue) {
            if (settings.userCurrencySymbolPosition == "left")
                lb_etc.text = "<b>" + settings.userCurrencySymbol + " " + (returnValue).toFixed(2) + "</b>"
            else
                lb_etc.text = "<b>" + (returnValue).toFixed(2) + " " + settings.userCurrencySymbol + "</b>"

            if(settings.showArrows)
            {
                if(curSettings.etc == returnValue)
                    lb_etcarrow.text = "\u2B0C"

                if(returnValue < curSettings.etc)
                    lb_etcarrow.text = "\u2B07"

                if(returnValue > curSettings.etc)
                    lb_etcarrow.text = "\u2B06"
            }
            else
                lb_etcarrow.text = ""

            if(curSettings.etc == returnValue)
                lb_etc.color = "#000000"

            if(returnValue < curSettings.etc)
                lb_etc.color = "#e60000"

            if(returnValue > curSettings.etc)
                lb_etc.color = "#009933"

            curSettings.etc = (returnValue).toFixed(2)
        })
        done()
    }

    function done() {
        console.log("-------------- updated all cryptoprices")
    }
}
