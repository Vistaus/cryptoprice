import requests

def get_latest_price(api, currencie, real):

    COINBASE_API_URL = "https://api.coinbase.com/v2/prices/%s-%s/spot" % (currencie, real)
    CRYPTOCOMPARE_API_URL = "https://min-api.cryptocompare.com/data/pricemultifull?fsyms=%s&tsyms=%s" % (currencie.upper(), real.upper())

    if api == "COINBASE":
        response = requests.get(COINBASE_API_URL)
        response_json = response.json()
        return float(response_json['data']['amount'])
    else:
        response = requests.get(CRYPTOCOMPARE_API_URL)
        response_json = response.json()
        return float(response_json['RAW'][currencie.upper()][real.upper()]['PRICE'])
